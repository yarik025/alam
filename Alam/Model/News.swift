//
//  News.swift
//  Alam
//
//  Created by Yaroslav Georgievich on 10.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import Foundation

struct ResponseJson: Codable {
    let status: String?
    let sources: [News]?
}

struct News: Codable {
    let id: String?
    let name: String?
    let description: String?
    let url: String?
    let category: String?
    let language: String?
    let country: String?
}
