//
//  ViewController.swift
//  Alam
//
//  Created by Yaroslav Georgievich on 10.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func presentButton(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondVC = storyBoard.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
//        self.navigationController?.pushViewController(secondVC, animated: true)
        self.present(secondVC, animated: true)
    }

}

