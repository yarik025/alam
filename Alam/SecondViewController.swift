//
//  SecondViewController.swift
//  Alam
//
//  Created by Yaroslav Georgievich on 10.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let webClient = WebClient()
    private var news = [News]()
    
    var urlString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "NEWS"
//        self.tableView.rowHeight = 120
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        getNews()
    }
    
    @objc func tapWebLabel(sender: UITapGestureRecognizer) {
        let webVC = storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.urlString = urlString
//        self.navigationController?.pushViewController(webVC, animated: true)
        self.present(webVC, animated: true)
    }
    
    func getNews() {
        
        guard let url = URL(string: "https://newsapi.org/v2/sources") else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("243a7a72dbea466280be05aac3ad1f8d", forHTTPHeaderField: "X-Api-Key")
        
        webClient.fetchDataFromApi(url: urlRequest) { (response, serviceError) in
            DispatchQueue.main.async {
                guard serviceError == nil else {
                    print("Service error when fetching the result")
                    return
                }
                
                guard let response = response else {
                    print("The data from server is nil")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    print(response)
                    let jsonData = try decoder.decode(ResponseJson.self, from: response)
                    
                    if jsonData.sources != nil {
                        self.news = jsonData.sources!
                        self.tableView.reloadData()
                        print("call api")
                    }
//                    print("jsonDATA: \(jsonData)")
//                    print("news: \(self.news)")
                } catch {
                    print("Caught an excepsion:")
                }
            }
        }
    }
}

extension SecondViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as? TableCell else { return UITableViewCell() }
        cell.titleLabel.text = news[indexPath.row].name ?? "-"
        cell.categoryLabel.text = news[indexPath.row].category ?? "-"
        cell.webStiteLabel.text = news[indexPath.row].url
        
        cell.webStiteLabel.textColor = UIColor.blue
        cell.categoryLabel.textColor = UIColor.red
        
        //Делает лейбл кликабельным
        let tap = UITapGestureRecognizer(target: self, action: #selector(SecondViewController.tapWebLabel))
        cell.webStiteLabel.isUserInteractionEnabled = true
        cell.webStiteLabel.addGestureRecognizer(tap)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
        return 135
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.urlString = news[indexPath.row].url ?? ""
//    }
}
