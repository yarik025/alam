//
//  WebViewController.swift
//  Alam
//
//  Created by Yaroslav Georgievich on 10.07.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    var urlString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(urlString)
        if let urlStr = urlString {
            if let url = URL(string: urlStr) {
                let urlRequest = URLRequest(url: url)
                webView.load(urlRequest)
            }
        }
    }
    

}
